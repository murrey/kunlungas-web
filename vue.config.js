const config = require("./src/config");
const webpack = require("webpack");
const TerserPlugin = require("terser-webpack-plugin");
const CompressionWebpackPlugin = require("compression-webpack-plugin");
let scssVariables = require("./src/styles/variables.scss.js");

module.exports = {
    lintOnSave: false,
    publicPath: '/kunlungasWeb/',
    // publicPath: process.env.NODE_ENV === 'production'
    //     ? '/kunlungasWeb/'
    //     : '',
    productionSourceMap: false,

    chainWebpack: (config) => {
        config.plugin("provide").use(webpack.ProvidePlugin, [
            {
                XE: "xe-utils",
            },
        ]);
        config.plugin("define").use(webpack.DefinePlugin, [
            {
                VE_ENV: {
                    MODE: JSON.stringify(process.env.NODE_ENV),
                },
            },
        ]);
        config.plugins.delete("prefetch");
    },

    configureWebpack: () => {
        let baseConfig = {};
        let envConfig = {};
        if (process.env.NODE_ENV === "production") {
            // 为生产环境修改配置...
            envConfig = {
                optimization: {
                    splitChunks: {
                        chunks: "all",
                        cacheGroups: {
                            echarts: {
                                name: "chunk-echarts",
                                priority: 20,
                                test: /[\\/]node_modules[\\/]_?echarts(.*)/,
                            },
                            elementPlus: {
                                name: "chunk-elementPlus",
                                priority: 20,
                                test: /[\\/]node_modules[\\/]_?element-plus(.*)/,
                            },
                            elementPlusIcon: {
                                name: "chunk-elementPlusIcon",
                                priority: 20,
                                test: /[\\/]node_modules[\\/]_?@element-plus[\\/]icons(.*)/,
                            },
                            mockjs: {
                                name: "chunk-mockjs",
                                priority: 20,
                                test: /[\\/]node_modules[\\/]_?mockjs(.*)/,
                            },
                        },
                    },
                },
                externals: {
                },
                plugins: [
                    new TerserPlugin({
                        terserOptions: {
                            compress: {
                                drop_console: true,
                                drop_debugger: true,
                            },
                        },
                    }),
                    new CompressionWebpackPlugin({
                        filename: "[path][base].gz",
                        algorithm: "gzip",
                        // test: /\.js$|\.html$|\.json$|\.css/,
                        test: /\.js$|\.json$|\.css/,
                        threshold: 10240, // 只有大小大于该值的资源会被处理
                        minRatio: 0.8, // 只有压缩率小于这个值的资源才会被处理
                    }),
                ],
            };
        }
        return Object.assign(baseConfig, envConfig);
    },
    devServer: {
        port: "15678", //代理端口
        open: true, //项目启动时是否自动打开浏览器
        proxy: {
            // "/api": {
            //     //代理api
            //     target: "http://192.168.1.6:15677", // 代理接口
            //     changeOrigin: true, //是否跨域
            //     ws: false, // proxy websockets
            //     pathRewrite: {
            //         //重写路径
            //         "^/api": "", //代理路径
            //     },
            // },
            // '/api/workflow': {//代理api
            //     target: "http://172.30.198.8:9890",// 代理接口
            //     // target: "http://192.168.136.63:9890/",// 代理接口
            //     changeOrigin: true,//是否跨域
            //     ws: false, // proxy websockets
            //     pathRewrite: {//重写路径
            //         "^/api/workflow": ''//代理路径
            //     }
            // },
            "/kunlungas": {
                //代理api
                // target: "http://119.96.172.48:15677", // 代理接口
                // target: "http://119.96.165.216:80/kunlungas",
                target: "http://192.168.1.2:30033",
                // target: "http://192.168.15.105:30033",
                // target: "http://192.168.136.43:9810",
                changeOrigin: true, //是否跨域
                ws: false, // proxy websockets
                pathRewrite: {
                    //重写路径
                    "^/kunlungas": "", //代理路径
                },
            },
        },
    },
    css: {
        loaderOptions: {
            scss: {
                // 注意：在 sass-loader v8 中，这个选项名是 "prependData"
                // additionalData: `@import "~@/styles/imports.scss";`
                additionalData: Object.keys(scssVariables)
                    .map((k) => `$${k.replace("_", "-")}: ${scssVariables[k]};`)
                    .join("\n"),
            },
        },
    },
};
