// 添加的地图json数据
import huangshi from './黄石市.json'
import daye from './大冶市.json'
import huangshigang from './黄石港区.json'
import tieshan from './铁山区.json'
import xisaishan from './西塞山区.json'
import xailu from './下陆区.json'
import yangxin from './阳新县.json'

// ...

const mapDict = {
	大冶市: 'daye',
	黄石港区: 'huangshigang',
	铁山区: 'tieshan',
	西塞山区: 'xisaishan',
	下陆区: 'xailu',
	阳新县: 'yangxin'
	// ...
}

const mapData = {
	daye,
	huangshigang,
	tieshan,
	xisaishan,
	xailu,
	yangxin
	// ...
}

export function getMap(mapName) {
	const cityName = mapDict[mapName]
	if (cityName) {
		return [cityName, mapData[cityName]]
	}
	return ['黄石市', huangshi]
}