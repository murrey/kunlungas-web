export default {
    path: "/",
    name: "AppMain",
    component: () => import("@/views/AppMain.vue"),
    redirect: { name: "Home2" },
    children: [
        {
            path: "infodisplay2",
            name: "InfoDisplay2",
            meta: { title: "信息展示" },
            component: () => import("@/views/infoDisplay2/infoDisplay.vue"),
        },
        {
            path: "/home2",
            name: "Home2",
            meta: { title: "首页" },
            component: () => import("@/views/home2.vue"),
        },
        {
            path: "/listinfo",
            name: "listinfo",
            meta: { title: "列表信息" },
            component: () => import("@/views/layoutpages/system/Menus.vue"),
        },
        {
            path: "/login",
            name: "Login",
            component: () => import("@/views/Login.vue"),
        },
    ],
};
