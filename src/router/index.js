/*
 * @Author: your name
 * @Date: 2021-01-07 11:41:32
 * @LastEditTime: 2021-03-24 16:14:18
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \vue3-element-admin\src\router\index.js
 */
import { createRouter, createWebHashHistory } from "vue-router";
import globalRoutes from "./globalRoutes";
import mainRoutes from "./mainRoutes";

import Login from '@/views/Login.vue'

const routes = [
    {
        path: '',
        redirect: { name: 'login' }
    },
    {
        path: '/login',
        name: 'login',
        component: Login
    },
    {
        path: "/index",
        name: "AppMain",
        component: () => import("@/views/AppMain.vue"),
        redirect: { name: "Home" },
        children: [
            {
                path: "/infodisplay2",
                name: "InfoDisplay2",
                meta: { title: "信息展示", keepAlive: false },
                component: () => import("@/views/infoDisplay2/infoDisplay.vue"),
            },
            {
                path: "/home2",
                name: "Home",
                meta: { title: "首页", keepAlive: true },
                component: () => import("@/views/home2.vue"),
            },
            {
                path: "/listinfo",
                name: "listinfo",
                meta: { title: "列表信息", keepAlive: false },
                component: () => import("@/views/layoutpages/system/Menus.vue"),
            },
            {
                path: "/workOrder",
                name: "workOrder",
                meta: { title: "工单系统", keepAlive: false },
                component: () => import("@/views/layoutpages/system/workOrder.vue"),
            },
            {
                path: "/userInfo",
                name: "userInfo",
                meta: { title: "用户管理", keepAlive: false },
                component: () => import("@/views/userInfo/userManger.vue"),
            },
            {
                path: "/regionalScheduleManagement",
                name: "regionalScheduleManagement",
                meta: { title: "区域数据统计", keepAlive: false },
                component: () => import("@/views/statistics/regionalScheduleManagement.vue"),
            },
            {
                path: "/personnelScheduleManagement",
                name: "personnelScheduleManagement",
                meta: { title: "人员进度统计", keepAlive: false },
                component: () => import("@/views/statistics/personnelScheduleManagement.vue"),
            },
        ],
    }
]


let router = createRouter({
    history: createWebHashHistory(),
    base: 'kunlungasWeb',
    scrollBehavior: () => ({ y: 0 }),
    isAddDynamicMenuRoutes: false, // 是否已经添加动态(菜单)路由
    routes: routes,
});
router.beforeEach((to, from, next) => {
    let getVal = localStorage.getItem("userName")
    if (!getVal && to.name != 'login') {
        next({ name: 'login' })
    } else {
        next()
    }

})
// 路由前置导航守卫
// router.beforeEach((to, from, next) => {
//     根据路由元信息设置文档标题
//     let getVal = localStorage.getItem("token")
//     if (!getVal) {
//         next('/login')
//     }

//     window.document.title = to.meta.title || admin
//     next()
// })
export default router;

// const routes = [
//     //一级路由
//     {
//         path: '/',
//         name: 'AppMain',
//         component: () => import("@/views/AppMain.vue"),
//         redirect: '/index/home2',
//         //路由嵌套
//         children: [
//             { path: '/index/home2', name: "Home2", component: () => import('@/views/home2') },
//             { path: '/index/listinfo', name: "listinfo", component: () => import('@/views/layoutpages/system/Menus.vue') },
//             { path: '/index/infodisplay2', name: "infodisplay2", component: () => import("@/views/infoDisplay2/infoDisplay.vue") }
//         ]
//     }
// ]

// const router = new createRouter({
//     history: createWebHashHistory(),
//     scrollBehavior: () => ({ y: 0 }),
//     isAddDynamicMenuRoutes: false, // 是否已经添加动态(菜单)路由
//     routes
// })

// export default router;